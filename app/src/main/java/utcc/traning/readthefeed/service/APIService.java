package com.test.helloworld.service;

import com.test.helloworld.datamodel.ResultData;
import com.test.helloworld.datamodel.ResultSingleData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface APIService {

    @GET("/api/v1/employees")
    public Call<ResultData> getListEmployee();

    @GET("/api/v1/employee/{id}")
    public Call<ResultSingleData> getEmployee(@Path("id") String id);


}

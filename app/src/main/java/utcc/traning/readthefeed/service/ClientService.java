package com.test.helloworld.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientService {
    public APIService getApiService() {
        return apiService;
    }

    private APIService apiService;
    public static ClientService Instance = new ClientService();
    private ClientService(){
        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl("http://dummy.restapiexample.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(APIService.class);
    }
}

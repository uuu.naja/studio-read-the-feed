package utcc.traning.readthefeed
import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import org.w3c.dom.Text
import utcc.traning.readthefeed.MainActivity
import utcc.traning.readthefeed.R

class Singup_Form : AppCompatActivity() {

    var txtFirstname: EditText? = null
    var txtLastname: EditText? = null
    var txtUsername: EditText? = null
    var txtbirthDate:TextView? = null
    var txtEmail: EditText? = null
    var txtphone: EditText? = null
    var txtPassword: EditText? = null
    var txtConfirmPassword: EditText? = null
    var btn_register: Button? = null
    var radioGenderMale: RadioButton? = null
    var radioGenderfeMale: RadioButton? = null
    var gender = ""
    var databasereference: DatabaseReference? = null
    var progressBar: ProgressBar? = null

    private var firebaseAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_singup__form)

        txtFirstname = findViewById(R.id.txt_firstname) as EditText
        txtLastname = findViewById(R.id.txt_lastname) as EditText
        txtUsername = findViewById(R.id.txt_username) as EditText
        txtbirthDate = findViewById(R.id.txt_birthDate) as TextView
        txtEmail = findViewById(R.id.txt_email) as EditText
        txtphone = findViewById(R.id.txt_email) as EditText
        txtPassword = findViewById(R.id.txt_password) as EditText
        txtConfirmPassword = findViewById(R.id.txt_confirm_password) as EditText
        btn_register = findViewById(R.id.btn_register) as Button
        radioGenderMale = findViewById(R.id.radio_male) as RadioButton
        radioGenderfeMale = findViewById(R.id.radio_female) as RadioButton
        progressBar = findViewById(R.id.progressBar) as ProgressBar

        firebaseAuth = FirebaseAuth.getInstance()

        databasereference = FirebaseDatabase.getInstance().getReference("user")

        btn_register?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val firstname = txtFirstname?.text.toString().trim { it <= ' ' }
                val lastname = txtLastname?.text.toString().trim { it <= ' ' }
                val username = txtUsername?.text.toString().trim { it <= ' ' }
                val birthDate = txtbirthDate?.text.toString().trim(){ it <= ' '}
                val email = txtEmail?.text.toString().trim { it <= ' ' }
                val phone = txtphone?.text.toString().trim(){ it <= ' '}
                val password = txtPassword?.text.toString().trim { it <= ' ' }
                val confirmpassword = txtConfirmPassword?.text.toString().trim { it <= ' ' }


                if (TextUtils.isEmpty(firstname)) {
                    Toast.makeText(this@Singup_Form, "Pleas Enter First name", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(lastname)) {
                    Toast.makeText(this@Singup_Form, "Pleas Enter Last name", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(username)) {
                    Toast.makeText(this@Singup_Form, "Pleas Enter Username", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(birthDate)) {
                    Toast.makeText(this@Singup_Form, "Pleas Enter Date", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(this@Singup_Form, "Pleas Enter Email", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(this@Singup_Form, "Pleas Enter Phone", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(this@Singup_Form, "Pleas Enter Password", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(confirmpassword)) {
                    Toast.makeText(this@Singup_Form, "Pleas Enter ConfirmPassword", Toast.LENGTH_SHORT).show()
                    return
                }

                if (password.length < 8) {
                    Toast.makeText(this@Singup_Form, "Password too short", Toast.LENGTH_SHORT).show()
                }

                if (radioGenderMale?.isChecked?:false) {
                    gender = "Male"
                }
                if (radioGenderfeMale?.isChecked?:false) {
                    gender = "Female"
                }

                progressBar?.visibility = View.VISIBLE

                if (password == confirmpassword) {
                    firebaseAuth!!.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(
                            this@Singup_Form,
                            object : OnCompleteListener<AuthResult> {
                                override fun onComplete(task: Task<AuthResult>) {
                                    progressBar?.visibility = View.GONE
                                    if (task.isSuccessful) {
                                        val information = User(
                                            firstname,lastname, username,birthDate, email,phone,gender
                                        )
                                        FirebaseDatabase.getInstance().getReference("User")
                                            .child(FirebaseAuth.getInstance().currentUser!!.uid)
                                            .setValue(information).addOnCompleteListener {
                                                Toast.makeText(
                                                    this@Singup_Form,
                                                    "Ragistration Complete",
                                                    Toast.LENGTH_SHORT
                                                ).show()

                                                startActivity(
                                                    Intent(
                                                        applicationContext,
                                                        Login_Form::class.java
                                                    )
                                                )
                                            }
                                    } else {
                                        Toast.makeText(
                                            this@Singup_Form,
                                            "Authentication Failed",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            })
                } else {
                    Toast.makeText(
                        this@Singup_Form,
                        "Those passwords didn't match.Try again",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    fun btncancel_singup_form(view: View) {
        startActivity(Intent(applicationContext, Login_Form::class.java))
    }
    fun date(view: View){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val dialog = DatePickerDialog(this)
            dialog.setOnDateSetListener(object:DatePickerDialog.OnDateSetListener{
                override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                    var date = "$dayOfMonth/$month/$year"
                    txtbirthDate?.setText(date)
                }

            })

                dialog.show()
        }
        return
    }
}

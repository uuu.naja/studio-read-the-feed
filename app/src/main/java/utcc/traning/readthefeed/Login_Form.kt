package utcc.traning.readthefeed

import androidx.appcompat.app.AppCompatActivity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast

import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import utcc.traning.readthefeed.MainActivity
import utcc.traning.readthefeed.R

class Login_Form : AppCompatActivity() {

    internal var txtemail: EditText?=null
    internal var txtpassword: EditText?=null
    internal var btn_login: Button?=null
    internal var progressBar: ProgressBar?=null
    internal var firebaseAuth: FirebaseAuth?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login__form)

        txtemail = findViewById(R.id.txt_emaillog)
        txtpassword = findViewById(R.id.txt_passwordlog)
        btn_login = findViewById(R.id.btn_login)
        progressBar = findViewById(R.id.progressBarlog)
        firebaseAuth = FirebaseAuth.getInstance()

        btn_login?.setOnClickListener(View.OnClickListener {
            val email = txtemail?.text.toString().trim { it <= ' ' }
            val password = txtpassword?.text.toString().trim { it <= ' ' }

            if (TextUtils.isEmpty(email)) {
                Toast.makeText(this@Login_Form, "Pleas Enter Email", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(this@Login_Form, "Pleas Enter Password", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            progressBar?.visibility = View.VISIBLE

            firebaseAuth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener { task ->
                progressBar?.visibility = View.GONE
                if (task.isSuccessful) {
                    Toast.makeText(this@Login_Form, "Logged in Successfully", Toast.LENGTH_SHORT)
                        .show()
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                } else {
                    Toast.makeText(this@Login_Form, "Error", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    fun btnreg_login_form(view: View) {
        startActivity(Intent(applicationContext, Singup_Form::class.java))
    }
}

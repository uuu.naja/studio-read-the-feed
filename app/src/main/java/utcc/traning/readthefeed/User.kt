package utcc.traning.readthefeed

class User {
    var firstname: String?=null
    var lastname: String?=null
    var username: String?=null
    var birthDate: String?=null
    var phone: String?=null
    var email: String?=null
    var gender: String?=null

    constructor() {

    }

    constructor(firstname: String,lastname: String, username: String,birthDate: String, email: String,phone: String, gender: String) {
        this.firstname = firstname
        this.lastname = lastname
        this.username = username
        this.birthDate = birthDate
        this.email = email
        this.phone = phone
        this.gender = gender
    }
}
